/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

gulp.task('default', function () {
    // place code for your default task here
});

gulp.task('sass', function (done) {
    gulp.src('./app/assets/sass/index.scss')
            .pipe(sass({
                errLogToConsole: true
            }))
            .pipe(rename({
                extname: '.min.css'
            }))
            .pipe(gulp.dest('./app/assets/css/'))
            .on('end', done);
});
