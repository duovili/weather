angular.module('Controller.Main', [])
        .controller('mainCtrl', function ($scope, $http) {
            $scope.gotData = false;

            $scope.getUserLocation = function()
            {
                $scope.gotData = false;
                navigator.geolocation.getCurrentPosition(function (success)
                {
                    $scope.error = false;
                    $scope.getLocationWeather(success.coords.latitude, success.coords.longitude)
                }, function (error)
                {
                    $scope.error = true;
                    $scope.gotError = error;
                    console.log(error);
                });
            };
            $scope.getLocationWeather = function(latitude, longitude)
            {
                var result = $http({
                    method: "POST",
                    dataType: 'json',
                    params: {
                        lat: latitude,
                        lon: longitude,
                        units : "metric",
                        APPID: '53f9d8e4213222cf517d86dc406d67fc'
                    },
                    url: "http://api.openweathermap.org/data/2.5/weather"
                });
                result.success(function (data)
                {
                    $scope.error = false;
                    $scope.weather = data;
                    $scope.gotData = true;
                    console.log(data);
                });
                result.error(function (error) {
                     $scope.gotData = false;
                    $scope.error = true;
                    $scope.gotError = error;
                    console.log(error);
                });
            }
            $scope.getUserLocation();
        });